name := "JWT"

version := "0.1"

scalaVersion := "2.12.7"

val Http4sVersion          = "0.18.19"
val CirceVersion           = "0.10.0"
val TsecVersion            = "0.0.1-M11"
val LogbackVersion         = "1.2.3"
val PureConfigVersion      = "0.9.2"
val FlywayVersion          = "4.2.0"
val DoobieVersion          = "0.5.3"
val MysqlVersion           = "5.1.46"
val H2Version              = "1.4.196"
val ScalaTestVersion       = "3.0.5"
val ScalaCheckVersion      = "1.14.0"

libraryDependencies ++= Seq(
  "org.http4s"            %% "http4s-blaze-server"   % Http4sVersion,
  "org.http4s"            %% "http4s-circe"          % Http4sVersion,
  "org.http4s"            %% "http4s-dsl"            % Http4sVersion,
  "io.circe"              %% "circe-generic"         % CirceVersion,
  "io.circe"              %% "circe-parser"          % CirceVersion,
  "io.circe"              %% "circe-literal"         % CirceVersion,
  "com.h2database"        %  "h2"                    % H2Version,
  "org.tpolecat"          %% "doobie-core"           % DoobieVersion,
  "org.tpolecat"          %% "doobie-h2"             % DoobieVersion,
  "org.tpolecat"          %% "doobie-scalatest"      % DoobieVersion,
  "org.tpolecat"          %% "doobie-hikari"         % DoobieVersion,
  "io.github.jmcardon"    %% "tsec-password"         % TsecVersion,
  "io.github.jmcardon"    %% "tsec-jwt-mac"          % TsecVersion,
  "io.github.jmcardon"    %% "tsec-http4s"           % TsecVersion,
  "io.github.jmcardon"    %% "tsec-mac"              % TsecVersion,
  "org.flywaydb"          %  "flyway-core"           % FlywayVersion,
  "com.github.pureconfig" %% "pureconfig"            % PureConfigVersion,
  "mysql"                 %  "mysql-connector-java"  % MysqlVersion ,
  "org.scalactic"         %% "scalactic"             % ScalaTestVersion,
  "org.scalatest"         %% "scalatest"             % ScalaTestVersion % Test,
  "org.scalacheck"        %% "scalacheck"            % ScalaCheckVersion % Test,
  "ch.qos.logback"        %  "logback-classic"       % LogbackVersion
)

scalacOptions ++= Seq("-language:higherKinds")

scalacOptions ++= Seq("-Ypartial-unification")