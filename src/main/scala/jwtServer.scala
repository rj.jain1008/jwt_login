import config.{DatabaseConfig, PetStoreConfig}
import domain.users._
import cats.effect._
import fs2.StreamApp.ExitCode
import fs2.{Stream, StreamApp}
import infrastructure.endpoint.UserEndpoints
import infrastructure.repository.doobie.DoobieUserRepositoryInterpreter
import org.http4s.server.blaze.BlazeBuilder
import tsec.mac.jca.HMACSHA256
import tsec.passwordhashers.jca.BCrypt

object jwtServer extends StreamApp[IO] {
  import scala.concurrent.ExecutionContext.Implicits.global

  override def stream(args: List[String], shutdown: IO[Unit]): Stream[IO, ExitCode] =
    createStream[IO](args, shutdown)

  private val keyGen = HMACSHA256

  def createStream[F[_]](args: List[String], shutdown: F[Unit])(
    implicit E: Effect[F]): Stream[F, ExitCode] =
    for {
      conf           <- Stream.eval(PetStoreConfig.load[F])
      signingKey     <- Stream.eval(keyGen.generateKey[F])
      xa             <- Stream.eval(DatabaseConfig.dbTransactor(conf.db))
      _              <- Stream.eval(DatabaseConfig.initializeDb(conf.db, xa))
      userRepo       =  DoobieUserRepositoryInterpreter[F](xa)
      userValidation =  UserValidationInterpreter[F](userRepo)
      userService    =  UserService[F](userRepo, userValidation)
      jwtStuff       =  JWTstuff(userRepo)
      exitCode       <- BlazeBuilder[F]
        .bindHttp(8080, "localhost")
        .mountService(UserEndpoints.endpoints(userService, jwtStuff, BCrypt.syncPasswordHasher[F]), "/")
        .serve
    } yield exitCode
}
