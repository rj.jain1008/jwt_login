package config

import cats.effect.Sync
import cats.implicits._
import pureconfig.error.ConfigReaderException

case class PetStoreConfig(db: DatabaseConfig, dbtest: DatabaseConfig)

object PetStoreConfig {

  import pureconfig._

  /**
    * Loads the pet store config using PureConfig.  If configuration is invalid we will
    * return an error.  This should halt the application from starting up.
    */
  def load[F[_]](implicit E: Sync[F]): F[PetStoreConfig] =
    E.delay(loadConfigOrThrow[PetStoreConfig]("petstore"))
}

