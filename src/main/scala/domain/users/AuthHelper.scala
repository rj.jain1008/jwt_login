package domain.users

import cats.data.OptionT
import cats.effect.Sync
import tsec.authentication.BackingStore
import tsec.authentication.credentials.{BCryptPasswordStore, RawCredentials}
import tsec.passwordhashers.PasswordHash
import tsec.passwordhashers.jca.BCrypt

import scala.collection.mutable

object AuthHelper {

  /** dummy factory for backing storage */
  def dummyBackingStore[F[_], I, A](getId: A => I)(implicit F: Sync[F]) =
    new BackingStore[F, I, A] {

      val dummyStore = mutable.HashMap[I, A]()

      def put(elem: A): F[A] = F.delay {
        dummyStore.put(getId(elem), elem)
        elem
      }

      def get(id: I): OptionT[F, A] =
        OptionT.fromOption[F](dummyStore.get(id))


      def update(v: A): F[A] = {
        F.delay {
          dummyStore.update(getId(v), v)
          v
        }
      }

      def delete(id: I): F[Unit] = F.delay {
        dummyStore.remove(id)
      }

    }

//  def PStore[F[_], I, A](getId: A => I)(implicit F: Sync[F]) =
//    new BCryptPasswordStore[F, I] {
//
//      private val otherTable = mutable.HashMap[I, PasswordHash[BCrypt]]()
//
//      def retrievePass(id: I): OptionT[F, PasswordHash[BCrypt]] =
//        OptionT.liftF {
//          otherTable.get(id) match {
//            case Some(s) => F.pure(s)
//            case None    => F.raiseError(new IllegalArgumentException)
//
//          }
//        }
//
//      def putCredentials(credentials: RawCredentials[I]): F[Unit] =
//        for {
//          hash <- BCrypt.hashpw[F](credentials.rawPassword)
//          _ <- F.delay(otherTable.put(credentials.identity, hash))
//        } yield ()

//    def updateCredentials(credentials: RawCredentials[I]): F[Unit] = F.delay {
//      otherTable.update(credentials.identity,
//        credentials.rawPassword.hashPassword[SCrypt])
//    }
//
//    def removeCredentials(credentials: RawCredentials[I]): F[Unit] =
//      F.ensure(retrievePass(credentials.identity))(new IllegalArgumentException)(
//        credentials.rawPassword.checkWithHash(_)) *> F.delay(
//        otherTable.remove(credentials.identity)) *> F.unit
//    }

}
