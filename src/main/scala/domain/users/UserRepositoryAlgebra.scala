package domain.users

import tsec.authentication.BackingStore

trait UserRepositoryAlgebra[F[_], Long, User] extends BackingStore[F, Long, User] {

    def findByUserName(userName: String): F[Option[User]]

    def deleteByUserName(userName: String): F[Option[Unit]]

}
