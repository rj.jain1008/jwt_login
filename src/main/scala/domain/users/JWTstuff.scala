package domain.users

import cats.Id
import cats.effect.Sync
import tsec.authentication.{JWTAuthenticator, SecuredRequestHandler, TSecJWTSettings}
import tsec.mac.jca.{HMACSHA256, MacSigningKey}

import scala.concurrent.duration._

class JWTstuff[F[_]: Sync] (userRepo: UserRepositoryAlgebra[F, Long, User]){

  val AccessTokenSettings = TSecJWTSettings("AccessToken", 10.minutes, Some(5.minutes))

  val signingKey: MacSigningKey[HMACSHA256] = HMACSHA256.generateKey[Id]

  val jwtStatelessAuth = JWTAuthenticator.unbacked.inHeader(
    settings = AccessTokenSettings,
    identityStore = userRepo,
    signingKey = signingKey
  )

  val Auth = SecuredRequestHandler(jwtStatelessAuth)
}

object JWTstuff {
  def apply[F[_]: Sync](userRepo: UserRepositoryAlgebra[F, Long, User]) = new JWTstuff(userRepo)
}
