package domain.users

import cats._
import cats.data.EitherT
import cats.implicits._
import domain.{UserAlreadyExistsError, UserNotFoundError}

class UserValidationInterpreter[F[_]: Monad](userRepo: UserRepositoryAlgebra[F, Long, User])
  extends UserValidationAlgebra[F] {

  def doesNotExist(user: User) = EitherT {
    userRepo.findByUserName(user.userName).map {
      case None => Right(())
      case Some(_) => Left(UserAlreadyExistsError(user))
    }
  }


  def exists(userId: Option[Long]): EitherT[F, UserNotFoundError.type, Unit] =
    EitherT {
      userId.map{id =>
        userRepo.get(id).value.map {
          case Some(_) => Right(())
          case _ => Left(UserNotFoundError)
        }
      }.getOrElse(
        Either.left[UserNotFoundError.type, Unit](UserNotFoundError).pure[F]
      )
    }
}

object UserValidationInterpreter {
  def apply[F[_]: Monad](userRepo: UserRepositoryAlgebra[F, Long, User]): UserValidationAlgebra[F] =
    new UserValidationInterpreter[F](userRepo)
}
