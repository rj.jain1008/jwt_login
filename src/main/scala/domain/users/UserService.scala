package domain.users

import cats._
import cats.data._
import cats.effect.Sync
import cats.syntax.functor._
import domain.{UserAlreadyExistsError, UserNotFoundError}
import tsec.authentication.credentials.BCryptPasswordStore
import tsec.authentication.{BackingStore, JWTAuthenticator, SecuredRequestHandler, TSecJWTSettings}
import tsec.mac.jca.{HMACSHA256, MacSigningKey}



class UserService[F[_]: Sync](userRepo: UserRepositoryAlgebra[F, Long, User], validation: UserValidationAlgebra[F]) {



  //  val jwtStore = dummyBackingStore[F, SecureRandomId, AugmentedJWT[HMACSHA256, String]](s => SecureRandomId.coerce(s.id))

//  val myPStore: BCryptPasswordStore[F, Long]  = PStore[F, Long, User](_.id.get)


  def createUser(user: User): EitherT[F, UserAlreadyExistsError, User] =
    for {
      _ <- validation.doesNotExist(user)
      saved <- EitherT.liftF(userRepo.put(user))
    } yield saved

  def getUser(userId: Long): EitherT[F, UserNotFoundError.type, User] =
    EitherT.fromOptionF(userRepo.get(userId).value, UserNotFoundError)

  def getUserByName(userName: String): EitherT[F, UserNotFoundError.type, User] =
    EitherT.fromOptionF(userRepo.findByUserName(userName), UserNotFoundError)

  def deleteUser(userId: Long): F[Unit] = userRepo.delete(userId).as(())

  def deleteByUserName(userName: String): F[Unit] =
    userRepo.deleteByUserName(userName).as(())

  def update(user: User): EitherT[F, UserNotFoundError.type, User] =
    for {
      _ <- validation.exists(user.id)
      saved <- EitherT.fromOptionF(OptionT.liftF(userRepo.update(user)).value, UserNotFoundError)
    } yield saved
}

object UserService {
  def apply[F[_]: Sync](repository: UserRepositoryAlgebra[F, Long, User], validation: UserValidationAlgebra[F]): UserService[F] =
    new UserService[F](repository, validation)
}
