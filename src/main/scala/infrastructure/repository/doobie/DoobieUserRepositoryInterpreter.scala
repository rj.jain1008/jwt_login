package infrastructure.repository.doobie

import cats._
import cats.data.OptionT
import cats.implicits._
import domain.users.{User, UserRepositoryAlgebra}
import doobie._
import doobie.implicits._

private object UserSQL {
  def insert(user: User): Update0 = sql"""
    INSERT INTO USERS (USER_NAME, FIRST_NAME, LAST_NAME, EMAIL, HASH, PHONE)
    VALUES (${user.userName}, ${user.firstName}, ${user.lastName}, ${user.email}, ${user.hash}, ${user.phone})
  """.update

  def update(user: User, id: Long): Update0 = sql"""
    UPDATE USERS
    SET FIRST_NAME = ${user.firstName}, LAST_NAME = ${user.lastName}, EMAIL = ${user.email}, HASH = ${user.hash}, PHONE = ${user.phone}
    WHERE ID = $id
  """.update

  def select(userId: Long): Query0[User] = sql"""
    SELECT USER_NAME, FIRST_NAME, LAST_NAME, EMAIL, HASH, PHONE, ID
    FROM USERS
    WHERE ID = $userId
  """.query[User]

  def byUserName(userName: String): Query0[User] = sql"""
    SELECT USER_NAME, FIRST_NAME, LAST_NAME, EMAIL, HASH, PHONE, ID
    FROM USERS
    WHERE USER_NAME = $userName
  """.query[User]

  def delete(userId: Long): Update0 = sql"""
    DELETE FROM USERS WHERE ID = $userId
  """.update

  def removeAll: Update0 = sql"""
     TRUNCATE TABLE USERS
  """.update

  val selectAll: Query0[User] = sql"""
    SELECT USER_NAME, FIRST_NAME, LAST_NAME, EMAIL, HASH, PHONE, ID
    FROM USERS
  """.query
}



class DoobieUserRepositoryInterpreter[F[_]: Monad](val xa: Transactor[F])
  extends UserRepositoryAlgebra[F, Long, User] {

  import UserSQL._

  def put(user: User): F[User] =
    insert(user).withUniqueGeneratedKeys[Long]("ID").map(id => user.copy(id = id.some)).transact(xa)

  def get(userId: Long): OptionT[F, User] = OptionT(select(userId).option.transact(xa))

  def update(user: User): F[User] = UserSQL.update(user, user.id.get).run.transact(xa).as(user)

  def delete(userId: Long): F[Unit] = UserSQL.delete(userId).run.transact(xa).as(Unit)

  def deleteAll: F[Unit] = UserSQL.removeAll.run.transact(xa).as(Unit)

  def findByUserName(userName: String): F[Option[User]] = byUserName(userName).option.transact(xa)

  def deleteByUserName(userName: String): F[Option[Unit]] = OptionT(findByUserName(userName)).mapFilter(_.id).semiflatMap(delete).value

}

object DoobieUserRepositoryInterpreter {
  def apply[F[_]: Monad](xa: Transactor[F]): DoobieUserRepositoryInterpreter[F] =
    new DoobieUserRepositoryInterpreter(xa)
}

