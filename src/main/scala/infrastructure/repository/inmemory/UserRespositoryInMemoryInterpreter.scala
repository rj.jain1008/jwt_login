package infrastructure.repository.inmemory

import java.util.Random

import cats.data.OptionT
import cats.implicits._
import cats.Monad
import domain.users.{User, UserRepositoryAlgebra}

import scala.collection.concurrent.TrieMap

class UserRepositoryInMemoryInterpreter[F[_]: Monad] extends UserRepositoryAlgebra[F, Long, User] {

  private val cache = new TrieMap[Long, User]

  private val random = new Random

  def put(user: User): F[User] = {
    val id = random.nextLong
    val toSave = user.copy(id = id.some)
    cache += (id -> toSave)
    toSave.pure[F]
  }

  def update(user: User): F[User] = user.id.foldM(user) {(user, id) =>
      cache.update(id, user)
      user.pure[F]
    }

  def get(id: Long): OptionT[F, User] = OptionT.apply(cache.get(id).pure[F])

  def delete(id: Long): F[Unit] = cache.remove(id).pure[F].as(Unit)

  def findByUserName(userName: String): F[Option[User]] =
    cache.values.find(u => u.userName == userName).pure[F]

  def deleteByUserName(userName: String): F[Option[Unit]] = {
    val deleted = for {
      user <- cache.values.find(u => u.userName == userName)
      userID <- user.id
      _ <- cache.remove(userID)
    } yield ()
    deleted.pure[F]
  }

}

object UserRepositoryInMemoryInterpreter {
  def apply[F[_]: Monad]() = new UserRepositoryInMemoryInterpreter[F]
}
