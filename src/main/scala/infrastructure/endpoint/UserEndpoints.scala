package infrastructure.endpoint

import cats.effect.{Effect, Sync}
import cats.implicits._
import domain._
import domain.authentication._
import domain.users._
import infrastructure.MiddleWare
import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.{EntityDecoder, HttpService, Response}
import tsec.authentication.{TSecAuthService, _}
import tsec.common.{VerificationFailed, VerificationStatus, Verified}
import tsec.mac.jca.HMACSHA256
import tsec.passwordhashers.jca.BCrypt
import tsec.passwordhashers.{PasswordHash, PasswordHasher}

import scala.language.higherKinds

class UserEndpoints[F[_], BCrypt](implicit F: Sync[F]) extends Http4sDsl[F] {
  /* Jsonization of our User type */

  implicit val userDecoder: EntityDecoder[F, User] = jsonOf
  implicit val loginReqDecoder: EntityDecoder[F, LoginRequest] = jsonOf

  implicit val signupReqDecoder: EntityDecoder[F, SignupRequest] = jsonOf

//  private def loginEndpoint(userService: UserService[F], cryptService: PasswordHasher[F, BCrypt]) : HttpService[F] =
//    HttpService[F] {
//      case request @ POST -> Root / "login" =>
//        val response =
//          for {
//            rawCreds <- OptionT.liftF(request.as[RawCredentials[String]])
//            u        <- userStore.get(rawCreds.identity)
//            isAuthed <- OptionT.liftF(myPStore.isAuthenticated(rawCreds))
//            _        <- if (isAuthed) OptionT.pure[F](()) else OptionT.none[F, Unit]
//            newAuth  <- OptionT.liftF(jwtStatefulAuth.create(u.phone))
//          } yield jwtStatefulAuth.embed(Response[F](), newAuth)
//
//        response
//          .getOrElse(
//            Response[F](Status.Unauthorized)
//          )
//    }

  def authedService: TSecAuthService[User, AugmentedJWT[HMACSHA256, Long], F] = TSecAuthService {
    //Where user is the case class User above
    case request@ GET -> Root / "api" asAuthed user =>

      for{
        response <- Ok(user.asJson)
      }yield response
    /*
    Note: The request is of type: SecuredRequest, which carries:
    1. The request
    2. The Authenticator (i.e token)
    3. The identity (i.e in this case, User)
     */
    //val r: SecuredRequest[F, User, AugmentedJWT[HMACSHA256, String]] = request

  }

  private def service(jwtStuff: JWTstuff[F]): HttpService[F] =
    jwtStuff.Auth.liftService(MiddleWare(jwtStuff, authedService))

  private def loginEndpoint(userService: UserService[F], jwtStuff: JWTstuff[F], cryptService: PasswordHasher[F, BCrypt]): HttpService[F] =
    HttpService[F] {
      case req @ POST -> Root / "login" =>
        for {
          login <- req.as[LoginRequest]
          name = login.userName
          maybeUser <- userService.getUserByName(name).value
          checkResult <-maybeUser match {
            case Right(user) => cryptService.checkpw(login.password, PasswordHash[BCrypt](user.hash))
            case Left(_) => VerificationFailed.pure[F].widen[VerificationStatus]
          }
          resp <-
            if(checkResult == Verified) maybeUser.fold(_ => BadRequest(), user => Ok(user.asJson))
            else maybeUser.fold(_ => BadRequest(s"Authentication failed for user $name"), _ => Ok())

          token1 <- jwtStuff.jwtStatelessAuth.create(maybeUser match {case Left(_) => 0
                                                             case  Right(user) => user.id.get})
        } yield jwtStuff.jwtStatelessAuth.embed(resp, token1)

//        action.value.flatMap {
//          case Right(user) => Ok(user.asJson)
//          case Left(UserAuthenticationFailedError(name)) => BadRequest(s"Authentication failed for user $name")
//        }
    }

  private val errorHandler: ValidationError => F[Response[F]] = {
    case UserAlreadyExistsError(existing) =>
      Conflict(s"The user ${existing.userName} already exists")
  }


  private def signupEndpoint(userService: UserService[F], jwtStuff: JWTstuff[F], cryptService: PasswordHasher[F, BCrypt]): HttpService[F] =
    HttpService[F] {
      case req @ POST -> Root / "users" =>
        for {
          signup <- req.as[SignupRequest]
          hash <- BCrypt.hashpw[F](signup.password)
          user <- signup.asUser(hash).pure[F]
          res <- userService.createUser(user).value
          token1 <- jwtStuff.jwtStatelessAuth.create(res match {
                                                                    case Left(_) => 0
                                                                    case Right(usr) => usr.id.get})
          result <- res.fold(errorHandler, saved => Ok(saved.asJson))
        } yield result match {
          case Ok(_) => jwtStuff.jwtStatelessAuth.embed(result, token1)
          case err => err
        }
    }

  private def updateEndpoint(userService: UserService[F]): HttpService[F] =
    HttpService[F] {
      case req @ PUT -> Root / "users" / name =>
        val action = for {
          user <- req.as[User]
          updated = user.copy(userName = name)
          result <- userService.update(updated).value
        } yield result

        action.flatMap {
          case Right(saved) => Ok(saved.asJson)
          case Left(UserNotFoundError) => NotFound("User not found")
        }
    }

  private def searchByNameEndpoint(userService: UserService[F]): HttpService[F] =
    HttpService[F] {
      case GET -> Root / "users" / userName =>
        userService.getUserByName(userName).value.flatMap {
          case Right(found) => Ok(found.asJson)
          case Left(UserNotFoundError) => NotFound("The user was not found")
        }
    }

  private def deleteUserEndpoint(userService: UserService[F]): HttpService[F] =
    HttpService[F] {
      case DELETE -> Root / "users" / userName =>
        for {
          _ <- userService.deleteByUserName(userName)
          resp <- Ok()
        } yield resp
    }


  def endpoints(userService: UserService[F], jwtStuff: JWTstuff[F], cryptService: PasswordHasher[F, BCrypt]): HttpService[F] =
    loginEndpoint(userService, jwtStuff, cryptService) <+>
      signupEndpoint(userService,jwtStuff, cryptService) <+>
      updateEndpoint(userService) <+>
      searchByNameEndpoint(userService)   <+>
      deleteUserEndpoint(userService) <+> service(jwtStuff)
}

object UserEndpoints {
  def endpoints[F[_]: Effect, BCrypt](
                                       userService: UserService[F],
                                       jwtStuff: JWTstuff[F],
                                       cryptService: PasswordHasher[F, BCrypt]
                                     ): HttpService[F] =
    new UserEndpoints[F, BCrypt].endpoints(userService, jwtStuff, cryptService)
}
