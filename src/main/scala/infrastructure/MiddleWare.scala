package infrastructure


import cats.data.{Kleisli, OptionT}
import cats.effect._
import cats.implicits._
import org.http4s._
import org.http4s.implicits._
import org.http4s.dsl.io._
import domain.users.{JWTstuff, User, UserService}
import org.http4s._
import tsec.authentication
import tsec.authentication.{AugmentedJWT, TSecAuthService}
import tsec.mac.jca.HMACSHA256



class MiddleWare[F[_]: Sync] (jwtStuff: JWTstuff[F]) {

  type MyTSec = TSecAuthService[User, AugmentedJWT[HMACSHA256, Long], F]

  def refreshToken(authService: MyTSec): MyTSec = Kleisli {
    req => for{
      resp <- authService(req)
    }yield jwtStuff.jwtStatelessAuth.embed(resp, req.authenticator)
  }


//  def apply(service: HttpService[IO], header: Header) =
//    service.map(addHeader(_, header))
}

object MiddleWare {
  def apply[F[_]: Sync](jwtStuff: JWTstuff[F], authService: TSecAuthService[User, AugmentedJWT[HMACSHA256, Long], F]) =
    new MiddleWare(jwtStuff).refreshToken(authService)
}

