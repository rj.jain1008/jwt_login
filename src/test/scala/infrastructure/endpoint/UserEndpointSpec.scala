package infrastructure.endpoint

import cats.effect._
import domain.authentication._
import domain.users._
import infrastructure.UserArbitraries
import infrastructure.repository.inmemory.UserRepositoryInMemoryInterpreter
import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s
import org.http4s._
import org.http4s.circe._
import org.http4s.dsl._
import org.scalatest._
import org.scalatest.fixture
import org.scalatest.prop.PropertyChecks
import tsec.passwordhashers.jca.BCrypt



class UserEndpointsSpec
  extends fixture.FunSuite
    with Matchers
    with PropertyChecks
    with UserArbitraries
    with Http4sDsl[IO] {

  val userRepo = UserRepositoryInMemoryInterpreter[IO]
  val userValidation =  UserValidationInterpreter[IO](userRepo)
  val userService = UserService[IO](userRepo, userValidation)
  val userHttpService =  UserEndpoints.endpoints(userService, BCrypt.syncPasswordHasher[IO])
  val req = Request[IO](Method.POST, Uri.uri("/users"))

  case class FixtureParam(userHttpService: http4s.HttpService[IO], req: Request[IO])

  def withFixture(test: OneArgTest): Outcome = {


    val theFixture = FixtureParam(userHttpService, req)

    try {
      withFixture(test.toNoArgTest(theFixture)) // "loan" the fixture to the test
    } //finally userRepo.deleteAll // clean up the fixture
  }

  test("create user") {f =>

      forAll { userSignup: SignupRequest =>
        IOAssertion {
          for {
            request <- f.req.withBody(userSignup.asJson)
            response <- f.userHttpService
              .run(request)
              .getOrElse(fail(s"Request was not handled: $request"))
          } yield {
            response.status shouldEqual Ok
          }
        }
      }
    }

  test("update user") {f =>
    implicit val userDecoder: EntityDecoder[IO, User] = jsonOf[IO, User]

    forAll { userSignup: SignupRequest =>
      IOAssertion{
        for {
          createRequest <- f.req.withBody(userSignup.asJson)
          createResponse <- f.userHttpService
            .run(createRequest)
            .getOrElse(fail(s"Request was not handled: $createRequest"))
          createdUser <- createResponse.as[User]
          userToUpdate = createdUser.copy(lastName = createdUser.lastName.reverse)
          updateRequest <- Request[IO](Method.PUT, Uri.unsafeFromString(s"/users/${createdUser.userName}"))
            .withBody(userToUpdate.asJson)
          updateResponse <- f.userHttpService
            .run(updateRequest)
            .getOrElse(fail(s"Request was not handled: $updateRequest"))
          updatedUser <- updateResponse.as[User]
        } yield {
          updateResponse.status shouldEqual Ok
          updatedUser.lastName shouldEqual createdUser.lastName.reverse
          createdUser.id shouldEqual updatedUser.id
        }
      }
    }
  }

  test("get user by userName") {f =>
    implicit val userDecoder: EntityDecoder[IO, User] = jsonOf[IO, User]

    forAll { userSignup: SignupRequest =>
      IOAssertion {
        for {
          createRequest <- f.req.withBody(userSignup.asJson)
          createResponse <- f.userHttpService
            .run(createRequest)
            .getOrElse(fail(s"Request was not handled: $createRequest"))
          createdUser <- createResponse.as[User]
          getRequest = Request[IO](Method.GET, Uri.unsafeFromString(s"/users/${createdUser.userName}"))
          getResponse <- f.userHttpService
            .run(getRequest)
            .getOrElse(fail(s"Get request was not handled"))
          getUser <- getResponse.as[User]
        } yield {
          getResponse.status shouldEqual Ok
          createdUser.userName shouldEqual getUser.userName
        }
      }
    }
  }


  test("delete user by userName") {f =>
    implicit val userDecoder: EntityDecoder[IO, User] = jsonOf[IO, User]

    forAll { userSignup: SignupRequest =>
      IOAssertion {
        for {
          createRequest <- f.req.withBody(userSignup.asJson)
          createResponse <- f.userHttpService
            .run(createRequest)
            .getOrElse(fail(s"Request was not handled: $createRequest"))
          createdUser <- createResponse.as[User]
          deleteRequest = Request[IO](Method.DELETE, Uri.unsafeFromString(s"/users/${createdUser.userName}"))
          deleteResponse <- f.userHttpService
            .run(deleteRequest)
            .getOrElse(fail(s"Delete request was not handled"))
          getRequest = Request[IO](Method.GET, Uri.unsafeFromString(s"/users/${createdUser.userName}"))
          getResponse <- f.userHttpService
            .run(getRequest)
            .getOrElse(fail(s"Get request was not handled"))
        } yield {
          createResponse.status shouldEqual Ok
          deleteResponse.status shouldEqual Ok
          getResponse.status shouldEqual NotFound
        }
      }
    }
   }
  }


