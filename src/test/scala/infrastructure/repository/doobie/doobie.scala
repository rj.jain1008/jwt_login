package infrastructure.repository

import cats.effect.IO
import config.{DatabaseConfig, PetStoreConfig}
import _root_.doobie.Transactor

package object doobie {
  def getTransactor : IO[Transactor[IO]] = for {
    conf <- PetStoreConfig.load[IO]
    tr <- DatabaseConfig.dbTransactor[IO](conf.dbtest)
    x <- DatabaseConfig.initializeDb(conf.dbtest, tr)
  } yield tr

  lazy val testTransactor : Transactor[IO] = getTransactor.unsafeRunSync()
}
