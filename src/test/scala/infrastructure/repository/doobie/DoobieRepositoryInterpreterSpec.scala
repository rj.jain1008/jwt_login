package infrastructure.repository.doobie

import cats.effect._
import domain.users._
import infrastructure.UserArbitraries
import infrastructure.endpoint.IOAssertion
import org.http4s
import org.http4s.Request
import org.http4s.dsl._
import org.scalatest._
import org.scalatest.prop.PropertyChecks

class DoobieRepositoryInterpreterSpec extends fixture.FunSuite
with Matchers
with PropertyChecks
with UserArbitraries
with Http4sDsl[IO] {



  case class FixtureParam(DoobieUserRepo: DoobieUserRepositoryInterpreter[IO])

  def withFixture(test: OneArgTest): Outcome = {

    val DoobieUserRepo = DoobieUserRepositoryInterpreter(testTransactor)
    val theFixture = FixtureParam(DoobieUserRepo)

    try {
      withFixture(test.toNoArgTest(theFixture)) // "loan" the fixture to the test
    }  finally DoobieUserRepo.deleteAll
  }

  test("insert user"){ f =>
    forAll{ user: User =>
      IOAssertion {
        for{
          createdUser <- f.DoobieUserRepo.put(user)
        }yield {
          createdUser.userName shouldEqual user.userName
          createdUser.firstName shouldEqual user.firstName
          createdUser.lastName shouldEqual user.lastName
          createdUser.hash shouldEqual user.hash
          createdUser.email shouldEqual user.email
          createdUser.phone shouldEqual user.phone
        }
      }
    }
  }

  test("update user"){ f =>
    forAll{ user: User =>
      IOAssertion {
        for{
          createdUser <- f.DoobieUserRepo.put(user)
          userToUpdate = createdUser.copy(lastName = createdUser.lastName.reverse)
          updatedUser <- f.DoobieUserRepo.update(userToUpdate)
        }yield {
          userToUpdate.lastName shouldEqual updatedUser.lastName
        }
      }
    }
  }

  test("search by username"){ f =>
    forAll{ user: User =>
      IOAssertion{
        for{
          createdUser <- f.DoobieUserRepo.put(user)
          searchedUser <- f.DoobieUserRepo.findByUserName(createdUser.userName)

        }yield{
          searchedUser.get.userName shouldEqual createdUser.userName
        }
      }
    }
  }

  test("delete by username"){ f =>
    forAll{ user: User =>
      IOAssertion{
        for{
          createdUser <- f.DoobieUserRepo.put(user)
          _ <- f.DoobieUserRepo.deleteByUserName(createdUser.userName)
          deletedUser <- f.DoobieUserRepo.findByUserName(createdUser.userName)
        }yield {
          deletedUser shouldEqual None
        }
      }
    }
  }

  test("delete user") { f =>
    forAll{ user: User =>
      IOAssertion  {
        for{
          createdUser <- f.DoobieUserRepo.put(user)
          _ <- f.DoobieUserRepo.delete(createdUser.id.get)
          deletedUser <- f.DoobieUserRepo.findByUserName(createdUser.userName)
        }yield {
          deletedUser shouldEqual None
        }
      }

    }
  }
}
