//package infrastructure.repository.doobie
//
//package $package$.repository
//
//import cats.effect.IO
//import doobie.util.transactor.Transactor
////import doobie.h2.H2Transactor
////import doobie.scalatest.IOChecker
////import doobie.util.transactor.Transactor
//import org.flywaydb.core.Flyway
//import org.scalatest.{BeforeAndAfterAll, FunSuiteLike}
//
//import scala.concurrent.ExecutionContext
//
// FIXME: IOChecker is outdated
////trait RepositorySpec extends FunSuiteLike with IOChecker with BeforeAndAfterAll {
//trait DoobieUserRepositorySpec extends FunSuiteLike with BeforeAndAfterAll {
//
//  val dbUrl   = "jdbc:h2:~/test"
//  val dbUser  = "sa"
//  val dbPass  = ""
//
//  val DoobieUserRepo = DoobieUserRepositoryInterpreter(testTransactor)
//
//  override protected def beforeAll(): Unit = {
//    super.beforeAll()
//    migrateDB.unsafeRunSync()
//  }
//
//  private val migrateDB: IO[Unit] =
//    IO {
//      val flyway = new Flyway
//      flyway.setDataSource(dbUrl, dbUser, dbPass)
//      flyway.migrate()
//    }
//
//}
//
