//package infrastructure.repository.doobie
//
//
//import cats.implicits._
//import cats.Monad
//import doobie._
//import doobie.implicits._
//
//private object test {
//
//  def removeAll: Update0 = sql"""
//     TRUNCATE TABLE USERS
//  """.update
//
//}
//
//class DoobieUserRepositoryInterpreter[F[_]: Monad](val xa: Transactor[F]) {
//
//  def deleteAll: F[Unit] = test.removeAll.run.transact(xa).as(Unit)
//
//}
//
